#!/usr/bin/make -f

include /usr/share/dpkg/pkg-info.mk

KOTLIN_VERSION_TO_BUILD := $(firstword $(subst +, ,$(DEB_VERSION)))
KOTLIN_VERSION_TO_USE := 1.3.31
COMMON_GRADLE_OPTIONS := -Dkotlin.compiler.execution.strategy=in-proces

%:
	dh $@ --buildsystem=gradle --with-maven-repo-helper

debian/kotlin.poms: debian/kotlin.poms.in
	sed -e "s|__VERSION__|$(KOTLIN_VERSION_TO_BUILD)|g" < $< > $@

override_dh_auto_build: debian/kotlin.poms
	# Build atomicfu component
	dh_auto_build -- --project-dir=atomicfu $(COMMON_GRADLE_OPTIONS) -Pkotlin_version=$(KOTLIN_VERSION_TO_USE) -Pversion=0.11.12 install

	# Build coroutines component (workaround for build failing the first time)
	-JDK_16=/usr/lib/jvm/java-8-openjdk-amd64/ dh_auto_build -- --project-dir=coroutines $(COMMON_GRADLE_OPTIONS) -Pkotlin_version=$(KOTLIN_VERSION_TO_USE) -PatomicFU_version=0.11.12 -Pversion=1.0.1 install
	# Build coroutines component
	JDK_16=/usr/lib/jvm/java-8-openjdk-amd64/ dh_auto_build -- --project-dir=coroutines $(COMMON_GRADLE_OPTIONS) -Pkotlin_version=$(KOTLIN_VERSION_TO_USE) -PatomicFU_version=0.11.12 -Pversion=1.0.1 install

	# Build Kotlin
	mkdir --parents buildSrc/prepare-deps/intellij-sdk/repo/kotlin.build.custom.deps/183.5153.4/intellijUltimate/lib
	mkdir --parents buildSrc/prepare-deps/intellij-sdk/repo/kotlin.build.custom.deps/183.5153.4/intellij-core
	mkdir --parents buildSrc/prepare-deps/intellij-sdk/repo/kotlin.build.custom.deps/183.5153.4/jps-standalone
	cp -u /usr/share/java/guava.jar							buildSrc/prepare-deps/intellij-sdk/repo/kotlin.build.custom.deps/183.5153.4/intellijUltimate/lib/guava-25.1-jre.jar
	cp -u /usr/share/java/jdom2-intellij.jar				buildSrc/prepare-deps/intellij-sdk/repo/kotlin.build.custom.deps/183.5153.4/intellijUltimate/lib/jdom.jar
	cp -u /usr/share/java/jna.jar							buildSrc/prepare-deps/intellij-sdk/repo/kotlin.build.custom.deps/183.5153.4/intellijUltimate/lib/jna.jar
	cp -u /usr/share/java/jna-platform.jar					buildSrc/prepare-deps/intellij-sdk/repo/kotlin.build.custom.deps/183.5153.4/intellijUltimate/lib/jna-platform.jar
	cp -u /usr/share/java/log4j-1.2.jar						buildSrc/prepare-deps/intellij-sdk/repo/kotlin.build.custom.deps/183.5153.4/intellijUltimate/lib/log4j.jar
	cp -u /usr/share/java/oro.jar							buildSrc/prepare-deps/intellij-sdk/repo/kotlin.build.custom.deps/183.5153.4/intellijUltimate/lib/oro-2.0.8.jar
	cp -u /usr/share/java/picocontainer-1.3.jar				buildSrc/prepare-deps/intellij-sdk/repo/kotlin.build.custom.deps/183.5153.4/intellijUltimate/lib/picocontainer-1.2.jar
	cp -u /usr/share/java/libtrove-intellij.jar				buildSrc/prepare-deps/intellij-sdk/repo/kotlin.build.custom.deps/183.5153.4/intellijUltimate/lib/trove4j.jar
	cp -u /usr/share/java/intellij-java-compatibility.jar	buildSrc/prepare-deps/intellij-sdk/repo/kotlin.build.custom.deps/183.5153.4/intellij-core/java-compatibility-1.0.1.jar
	cp -u /usr/share/java/streamex.jar						buildSrc/prepare-deps/intellij-sdk/repo/kotlin.build.custom.deps/183.5153.4/intellijUltimate/lib/streamex-0.6.7.jar
	cp -u /usr/share/java/lz4-java-1.5.1.jar				buildSrc/prepare-deps/intellij-sdk/repo/kotlin.build.custom.deps/183.5153.4/intellijUltimate/lib/lz4-1.3.0.jar
	cp -u /usr/share/java/guava.jar							buildSrc/prepare-deps/intellij-sdk/repo/kotlin.build.custom.deps/183.5153.4/intellijUltimate/lib/guava-25.1-jre.jar
	cp -u /usr/share/java/intellij-util-all.jar				buildSrc/prepare-deps/intellij-sdk/repo/kotlin.build.custom.deps/183.5153.4/intellijUltimate/lib/util.jar
	cp -u /usr/share/java/intellij-jps-model-all.jar		buildSrc/prepare-deps/intellij-sdk/repo/kotlin.build.custom.deps/183.5153.4/jps-standalone/jps-model.jar
	cp -u /usr/share/java/intellij-extensions.jar			buildSrc/prepare-deps/intellij-sdk/repo/kotlin.build.custom.deps/183.5153.4/intellijUltimate/lib/extensions.jar
	cp -u /usr/share/java/intellij-platform-api.jar			buildSrc/prepare-deps/intellij-sdk/repo/kotlin.build.custom.deps/183.5153.4/intellijUltimate/lib/platform-api.jar
	cp -u /usr/share/java/intellij-platform-impl.jar		buildSrc/prepare-deps/intellij-sdk/repo/kotlin.build.custom.deps/183.5153.4/intellijUltimate/lib/platform-impl.jar
	cp -u /usr/share/java/intellij-core.jar					buildSrc/prepare-deps/intellij-sdk/repo/kotlin.build.custom.deps/183.5153.4/intellij-core/intellij-core.jar

	ant -f ./debian/buildprotobufkotlin.xml
	cp ./debian/protobuf-debian.pom ./debian/maven-repo-cache/org/jetbrains/kotlin/protobuf/protobuf/debian/protobuf-debian.pom
	dh_auto_build -- $(COMMON_GRADLE_OPTIONS) -PkotlinVersionToBuild=$(KOTLIN_VERSION_TO_BUILD) -PbuildSrc.kotlin.version=$(KOTLIN_VERSION_TO_USE) -Pbootstrap.kotlin.version=$(KOTLIN_VERSION_TO_USE) dist install

	dh_auto_configure --buildsystem=maven
	JDK_18=/usr/lib/jvm/java-8-openjdk-amd64/ dh_auto_build --buildsystem=maven -- --file=libraries/pom.xml package

execute_before_dh_clean: debian/kotlin.poms
	dh_auto_build -- --project-dir=atomicfu $(COMMON_GRADLE_OPTIONS) -Pkotlin_version=$(KOTLIN_VERSION_TO_USE) -Pversion=0.11.12 clean
	# coroutines build system requires atomicfu-gradle-plugin from previous
	# step. Even when clean target is run, it needs a fully build dependency
	# readily available. For now, avoid building the dependency just to run
	# clean on this one.
	#dh_auto_build -- --project-dir=coroutines -Pkotlin_version=$(KOTLIN_VERSION_TO_USE) -PatomicFU_version=0.11.12 -Pversion=1.0.1 clean
	dh_auto_build -- $(COMMON_GRADLE_OPTIONS) -PkotlinVersionToBuild=$(KOTLIN_VERSION_TO_BUILD) -PbuildSrc.kotlin.version=$(KOTLIN_VERSION_TO_USE) -Pbootstrap.kotlin.version=$(KOTLIN_VERSION_TO_USE) clean
	# Compensate for not running clean on coroutines component and other
	# potential left outs. .gitignore on upstream project has 'build/' as a
	# pattern, so this should be safe.
	find buildSrc -name 'build' -and -type d | xargs rm -rf
	# Compensate for not being able to run maven with 'clean' as goal.
	find libraries -name target -and -type d | xargs rm -rf
	# Undo a file edit done by the Kotlin build system but not cleaned
	sed --in-place --regexp-extended --expression='s|(val CURRENT: KotlinVersion = KotlinVersion)\(([[:digit:]]+), ([[:digit:]]+), ([[:digit:]]+)\)|\1(\2, \3, 0)|' libraries/stdlib/src/kotlin/util/KotlinVersion.kt
	mh_unpatchpoms --package=kotlin
	rm -rf ./debian/kotlin.poms

execute_before_dh_install:
	mh_install

override_dh_auto_test:
